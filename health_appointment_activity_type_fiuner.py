# -*- coding: utf-8 -*-

##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date


from trytond.model import ModelView, ModelSingleton, Workflow, ModelSQL, fields, Unique
from trytond.transaction import Transaction
from trytond import backend
from trytond.pyson import Eval, Not, Bool, Equal, And, Or, If
from trytond.pool import Pool, PoolMeta

__all__ = ['Appointment']

    
class Appointment(metaclass=PoolMeta):
    'Patient Appointments'
    __name__ = 'gnuhealth.appointment'
    
    activity_type = fields.Selection([
        (None, ''),
        ('individual','Actividad Asistencial Individual'),
        ('groupal','Actividad Asistencial Grupal'),
        ('institutional','Actividad Institucional'),
        ('community','Actividad Comunitaria'),
        ],'Tipo de actividad',sort=False, help='Tipo de actividad referido al\n'\
                                                +'servicio/dispositivo correspondiente')
    
    individual_activity = fields.Selection([
        (None,''),
        ('a1', u'A.1. Entrevista de admisión y orientación'),
        ('a2', u'A.2. Entrevista o consulta'),
        ('a3', u'A.3. Entrevista Familiar'),
        ('a4', u'A.4. Intervención domiciliaria'),
        ('a5', u'A.5. Acompañamiento Terapéutico'),
        ('a6', u'A.6. Interconsulta'),
        ('a7', u'A.7. Intervención de Urgencia'),
        ('a8', u'A.8. Evaluaciones y Contactos para fines administrativos'),
        ('a9', u'A.9. Actividad Inter o Intrainstitucional'),
        ('a10', u'A.10. Reunión de equipo por usuario'),
        ('a11', u'A.11. Otras'),
        ],'Actividad Individual',sort=False,
            states={
                'invisible':Eval('activity_type')!='individual',
                'required':Eval('activity_type')=='individual',
                })
            
    groupal_activity = fields.Selection([
        (None,''),
        ('b1', u'B.1. Evaluación y orientación de la demanda grupal'),
        ('b2', u'B.2. Grupo Terapéutico'),
        ('b3', u'B.3. Taller'),               
        ('b4', u'B.4. Asamblea'),
        ('b5', u'B.5. Otras'),
        ],'Actividad Grupal',sort=False,
            states={
                'invisible':Eval('activity_type')!='groupal',
                'required':Eval('activity_type')=='groupal',
                })

    institutional_activity = fields.Selection([
        (None,''),
        ('c1', u'C.1. Actividad interdisciplinaria e intersectorial'),
        ('c2', u'C.2. Formación del Equipo de Salud Mental'),
        ('c3', u'C.3. Reunión Equipo de Salud Mental'),               
        ('c4', u'C.4. Docencia y Asesoramiento'),
        ('c5', u'C.5. Otras'),
        ],'Actividad Institucional',sort=False,
            states={
                'invisible':Eval('activity_type')!='institutional',
                'required':Eval('activity_type')=='institutional',
                })
            
    community_activity = fields.Selection([
        (None,''),
        ('d1', u'D.1. Actividad interdisciplinaria e intersectorial'),
        ('d2', u'D.2. Formación del Equipo de Salud Mental'),
        ('d3', u'D.3. Otras'),        
        ],'Actividad Comunitaria',sort=False,
            states={
                'invisible':Eval('activity_type')!='community',
                'required':Eval('activity_type')=='community',
                })
    
    def get_dni(self,name):
        if self.patient:
            return self.patient.puid
        return None      
        
    def get_hc(self,name):
        if self.patient:
            return self.patient.hc
        return None
    
    @staticmethod
    def default_activity_type():
        return 'individual'
    
    @staticmethod
    def default_individual_activity():
        return 'a2'
